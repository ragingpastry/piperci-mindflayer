## [1.0.1](https://gitlab.com/ragingpastry/piperci-mindflayer/compare/v1.0.0...v1.0.1) (2019-09-10)


### Bug Fixes

* test minor version bump ([dcfb63f](https://gitlab.com/ragingpastry/piperci-mindflayer/commit/dcfb63f))
